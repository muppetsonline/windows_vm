terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.64.0"
    }
  }
}

provider "azurerm" {
  # Configuration options
  features {}
}

resource "azurerm_resource_group" "vm-rg" {
  location = var.location
  name = "${var.app_name}-${var.role_name}-vm-${var.location}"
}

resource "azurerm_network_interface" "vm-nic" {
  count               = var.vm_count
  name                = "${var.app_name}-vm-nic${count.index}"
  location            = var.location
  resource_group_name = azurerm_resource_group.vm-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = "${var.virtual_network_id}/subnets/default"
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_availability_set" "vm-availset" {
  resource_group_name = azurerm_resource_group.vm-rg.name
  location = var.location
  name = "${var.app_name}-${var.role_name}-availset"
  platform_fault_domain_count = 2
}

resource "azurerm_windows_virtual_machine" "vm" {
  count               = var.vm_count
  name                = "${var.app_name}-${var.role_name}-vm${count.index}"
  resource_group_name = azurerm_resource_group.vm-rg.name
  location            = var.location
  size                = var.vm_size
  availability_set_id = azurerm_availability_set.vm-availset.id
  admin_username      = "adminuser"
  admin_password      = "P@$$w0rd1234!"
  network_interface_ids = [
    azurerm_network_interface.vm-nic[count.index].id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}